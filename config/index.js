module.exports = {
  store: {
    name: 'ed coffee 본점에 오신 것을 환영합니다 영업시간 00:00 ~ 00:01 ' +
      '하루 1분만 영업합니다 평일 및 공휴일 휴무',
    // The images in the config are considered as an 'external source' -
    // hence, it should not be bundled in the package.
    // Instead, it should be loaded from an external source, like Amazon S3,
    // etc.
    images: [
      'https://ftp.kkiro.kr/public/images/sample/coffee1.jpg',
      'https://ftp.kkiro.kr/public/images/sample/win31.png',
      'https://ftp.kkiro.kr/public/images/sample/win72.png',
      'https://ftp.kkiro.kr/public/images/sample/wine1.jpg',
    ],
    givenPoints: 100,
    coupons: [{
      points: 120,
      thumbnail: 'https://ftp.kkiro.kr/public/images/sample/americano1.jpg',
      name: '아메리카노아메리카노아메리카노시럽추가벤티샷추가샷추가샷추가',
    }, {
      points: 300,
      thumbnail: 'https://ftp.kkiro.kr/public/images/sample/win31.png',
      name: '한글 윈도우 3.1',
    }],
    language: 'ko',
  },
};
