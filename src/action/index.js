import validatePhone from '../util/validatePhone';

// These functions manipulate the state of the application.
// If we're using Redux, these would create action object, and actual
// changes will be made by reducers.
// However, since the application is relatively simple, we're just using
// functions to mutate the state.

export const init = () => ({
  page: 'splash', form: {}, store: null, error: null, loading: 0,
});

export const fetchStoreInfo = () => async(dispatch, state, props) => {
  const { connector } = props;
  const store = await connector.fetchStoreInfo();
  return { store };
};

export const start = () => (_, state) =>
  ({ page: 'form', form: { points: state.store.givenPoints }, result: {} });

export const reset = () => ({ page: 'splash' });

export const submit = phone => async(dispatch, state, props) => {
  if (!validatePhone(phone)) {
    dispatch(showError({ translate: 'phoneFormPhoneError' }, 3000));
    return {};
  }
  const { connector } = props;
  const { points } = state.form;
  const result = await connector.collectPoint(phone, points);
  return {
    page: 'result',
    result: Object.assign({}, result, { givenPoints: points }),
  };
};

export const showError = (message, autoClose) => ({
  error: {
    open: true,
    message,
    closeAt: autoClose && Date.now() + autoClose,
  },
});

export const closeError = () => (dispatch, state) => ({
  error: Object.assign({}, state.error, { open: false }),
});

export const startLoad = () => (dispatch, state) => ({
  loading: state.loading + 1,
});

export const stopLoad = () => (dispatch, state) => ({
  loading: Math.max(0, state.loading - 1),
});
