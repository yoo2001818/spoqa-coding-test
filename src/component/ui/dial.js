import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import '../../style/component/ui/dial.css';

function range(size) {
  let output = [];
  for (let i = 0; i < size; ++i) output.push(i);
  return output;
}

export default function Dial({ className, map, render }) {
  return (
    <div className={classNames('dial-component', className)}>
      { range(4).map(y => (
        <div className='row' key={y}>
          { range(3).map(x => (
            <div className='cell' key={x}>
              { render(map[y * 3 + x]) }
            </div>
          )) }
        </div>
      )) }
    </div>
  );
}

Dial.propTypes = {
  className: PropTypes.string,
  map: PropTypes.array.isRequired,
  render: PropTypes.func.isRequired,
};
