export { default as CancelIcon } from
  '!svg-react-loader?name=CancelIcon!../../image/cancel.svg';
export { default as BackIcon } from
  '!svg-react-loader?name=BackIcon!../../image/back.svg';
export { default as NoticeIcon } from
  '!svg-react-loader?name=NoticeIcon!../../image/notice.svg';
