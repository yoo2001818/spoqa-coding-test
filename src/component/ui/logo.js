import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import logoImage from '../../image/logo.png';

export default function Logo({ className }) {
  return (
    <img className={classNames('app-logo', className)}
      alt='도도포인트 로고' src={logoImage} />
  );
}

Logo.propTypes = {
  className: PropTypes.string,
};
