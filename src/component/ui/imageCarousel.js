import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Slider from 'react-slick';

import 'slick-carousel/slick/slick.css';

import '../../style/component/ui/imageCarousel.css';

export default function ImageCarousel(props) {
  const { images, className, arrows = false } = props;
  return (
    <Slider {...props} arrows={arrows}
      className={classNames('image-carousel-component', className)}
    >
      { images.map((v, i) => (
        <div key={i} className='image-carousel-item' style={{
          backgroundImage: `url(${v})`,
        }} />
      )) }
    </Slider>
  );
}

ImageCarousel.propTypes = {
  images: PropTypes.array.isRequired,
  className: PropTypes.string,
};
