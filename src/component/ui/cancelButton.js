import React from 'react';
import PropTypes from 'prop-types';

import __ from '../../lang';

import '../../style/component/ui/cancelButton.css';

import { CancelIcon } from './icon';

export default function CancelButton(props) {
  return (
    <button {...props} className='cancel-button-component'>
      { __('cancel') }
      <CancelIcon className='cancel-icon' />
    </button>
  );
}

CancelButton.propTypes = {
  onClick: PropTypes.func,
};
