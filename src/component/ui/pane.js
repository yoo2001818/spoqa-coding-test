import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import '../../style/component/ui/pane.css';

export default function Pane({ children, className }) {
  return (
    <div className={classNames('pane-component', className)}>
      { children }
    </div>
  );
}

Pane.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
};
