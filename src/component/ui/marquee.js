import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import '../../style/component/ui/marquee.css';

// The marquee operates by specifying 'keyframes' while running CSS
// transitions. This is done to show 60fps smooth animation while supporting
// varying width of content. Normal CSS animation doesn't support transition
// to automatically change its delay by its content width, so this approach
// is required to simulate HTML marquee tag.
export default class Marquee extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { offset: 0 };
    this.interval = null;
    this.updateAnimation = this.updateAnimation.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
  }
  componentDidMount() {
    this.handleUpdate();
    window.addEventListener('resize', this.handleUpdate);
  }
  componentDidUpdate() {
    this.handleUpdate();
  }
  componentWillUnmount() {
    this.endAnimation();
    window.removeEventListener('resize', this.handleUpdate);
  }
  handleUpdate() {
    this.measureText();
    if (this.textWidth > this.containerWidth) {
      this.startAnimation();
    } else {
      this.endAnimation();
      this.setState({ offset: 0, reset: true });
    }
  }
  startAnimation() {
    if (this.interval != null) return;
    this.interval = setInterval(this.updateAnimation, 500);
  }
  endAnimation() {
    if (this.interval == null) return;
    clearInterval(this.interval);
    this.interval = null;
  }
  updateAnimation() {
    const { speed = 100 } = this.props;
    let { offset } = this.state;
    let reset = false;
    offset -= speed;
    if (offset < -this.textWidth - speed) {
      reset = true;
      offset = this.containerWidth;
    }
    this.setState({ offset, reset });
  }
  measureText() {
    this.containerWidth = this.containerNode.clientWidth;
    this.textWidth = this.contentNode.scrollWidth;
  }
  render() {
    const { children, className } = this.props;
    const { offset, reset } = this.state;
    return (
      <div className={classNames('marquee-component', className)}
        ref={node => this.containerNode = node}
      >
        <div className={classNames('content', { reset })}
          ref={node => this.contentNode = node}
          style={{ transform: `translateX(${offset}px)` }}
        >
          { children }
        </div>
      </div>
    );
  }
}

Marquee.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  speed: PropTypes.number,
};
