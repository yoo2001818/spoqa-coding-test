import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import '../../style/component/ui/header.css';

export default function Header({ children, right, className }) {
  return (
    <div className={classNames('header-component', className)}>
      <div className='content'>
        { children }
      </div>
      { right && (
        <div className='right'>
          { right }
        </div>
      ) }
    </div>
  );
}

Header.propTypes = {
  children: PropTypes.node,
  right: PropTypes.node,
  className: PropTypes.string,
};
