import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import '../../style/component/ui/button.css';

export default function Button(props) {
  return (
    <button {...props}
      className={classNames('button-component', props.className)} />
  );
}

Button.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  onClick: PropTypes.func,
};
