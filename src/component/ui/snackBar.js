import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import '../../style/component/ui/snackBar.css';

import { NoticeIcon } from './icon';

export default function SnackBar({ children, hidden, className }) {
  return (
    <div className={classNames('snack-bar-component', { hidden }, className)}>
      <div className='content'>
        <NoticeIcon className='notice-icon' />
        { children }
      </div>
    </div>
  );
}

SnackBar.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  hidden: PropTypes.bool,
};
