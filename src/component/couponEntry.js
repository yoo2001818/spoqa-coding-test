import React from 'react';
import PropTypes from 'prop-types';

import __ from '../lang';

import '../style/component/couponEntry.css';

import Button from './ui/button';
import Pane from './ui/pane';
import PointSpan from './pointSpan';

function renderCol(text, index) {
  return (
    <span className='plain-text' key={index}>{ text }</span>
  );
}

export default function CouponEntry({ currentPoints, coupon, onUse }) {
  return (
    <Pane className='coupon-entry'>
      <div className='thumbnail' style={{
        backgroundImage: `url(${coupon.thumbnail})`,
      }} />
      { currentPoints >= coupon.points ? (
        <div className='content available'>
          <div className='header text'>
            <div className='row' key={0}>
              { __('resultCouponAvailable', [
                <span className='name' key='name'>{ coupon.name }</span>,
              ], false, renderCol) }
            </div>
          </div>
          <div className='footer'>
            <Button className='use grey-ui' onClick={onUse}>
              { __('resultCouponUse') }
            </Button>
          </div>
        </div>
      ) : (
        <div className='content tease'>
          <div className='text'>
            { __('resultCouponTease', [
              <div className='row' key={0}>
                { __('resultCouponTeasePoints', [
                  <PointSpan points={coupon.points - currentPoints}
                    key='points' />,
                ], false, renderCol) }
              </div>,
              <div className='row' key={1}>
                { __('resultCouponTeaseName', [
                  <span className='name' key='name'>{ coupon.name }</span>,
                ], false, renderCol) }
              </div>,
            ], false) }
          </div>
        </div>
      ) }
    </Pane>
  );
}

CouponEntry.propTypes = {
  currentPoints: PropTypes.number,
  coupon: PropTypes.object.isRequired,
  onUse: PropTypes.func,
};
