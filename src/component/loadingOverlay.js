import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import __ from '../lang';

import '../style/component/loadingOverlay.css';

export default function LoadingOverlay({ hidden }) {
  return (
    <div className={classNames('loading-overlay-component', { hidden })}>
      <p className='text'>
        { __('loadingText') }
      </p>
    </div>
  );
}

LoadingOverlay.propTypes = {
  hidden: PropTypes.bool,
};
