import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import formatPhone from '../util/formatPhone';

import '../style/component/phoneInput.css';

import Dial from './ui/dial';
import Button from './ui/button';
import { BackIcon } from './ui/icon';

const DIAL_MAP = [1, 2, 3, 4, 5, 6, 7, 8, 9, 'backspace', 0, 'done'];

export default class PhoneInput extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { value: props.defaultValue || '' };
    this.handleKeyDown = this.handleKeyDown.bind(this);
  }
  componentDidMount() {
    if (this.props.registerWindowKeyEvent) {
      window.addEventListener('keydown', this.handleKeyDown);
    }
  }
  componentWillUnmount() {
    if (this.props.registerWindowKeyEvent) {
      window.removeEventListener('keydown', this.handleKeyDown);
    }
  }
  handleKeyDown(e) {
    // Only forward digits, backspace
    if ((e.keyCode >= 48 && e.keyCode <= 57) || e.keyCode === 8) {
      this.input.focus();
    }
  }
  handleSubmit(e) {
    e.preventDefault();
    this.props.onSubmit(this.getValue());
  }
  getValue() {
    return this.props.value != null ? this.props.value : this.state.value;
  }
  triggerChange(input) {
    let newValue = formatPhone(input);
    const { value, onChange } = this.props;
    if (onChange != null) onChange(newValue);
    if (value == null) this.setState({ value: newValue });
  }
  handleChange(e) {
    this.triggerChange(e.target.value);
  }
  handleKey(key) {
    this.triggerChange(this.getValue() + key.toString());
  }
  handleBackspace() {
    this.triggerChange(this.getValue().slice(0, -1));
  }
  render() {
    const { className, caption, doneLabel } = this.props;
    const value = this.getValue();
    return (
      <div className={classNames('phone-input-component', className)}>
        <div className='header'>
          <form className='form' onSubmit={this.handleSubmit.bind(this)}>
            <input className='input' type='text' value={value}
              ref={node => this.input = node}
              onChange={this.handleChange.bind(this)} />
            <div className='cover' />
          </form>
          { caption && (
            <div className='caption'>
              { caption }
            </div>
          ) }
        </div>
        <Dial className='dial' map={DIAL_MAP}
          render={num => {
            if (num === 'done') {
              return (
                <Button onClick={this.handleSubmit.bind(this)}>
                  { doneLabel || 'Done' }
                </Button>
              );
            }
            if (num === 'backspace') {
              return (
                <Button className='white'
                  onClick={this.handleBackspace.bind(this)}
                >
                  <BackIcon className='backspace-icon' />
                </Button>
              );
            }
            return (
              <Button className='white'
                onClick={this.handleKey.bind(this, num)}
              >
                { num }
              </Button>
            );
          }}
        />
      </div>
    );
  }
}

PhoneInput.propTypes = {
  defaultValue: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  onSubmit: PropTypes.func,
  className: PropTypes.string,
  caption: PropTypes.node,
  doneLabel: PropTypes.node,
  registerWindowKeyEvent: PropTypes.bool,
};
