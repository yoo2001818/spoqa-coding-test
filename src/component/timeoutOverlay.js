import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import __ from '../lang';

import '../style/component/timeoutOverlay.css';

export default class TimeoutOverlay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expireAt: Date.now() + props.timeout,
    };
    this.triggered = false;
    this.handleEvent = this.handleEvent.bind(this);
  }
  handleEvent() {
    this.triggered = false;
    this.setState({
      expireAt: Date.now() + this.props.timeout,
    });
  }
  componentDidMount() {
    this.interval = setInterval(() => {
      if (Date.now() > this.state.expireAt && !this.triggered) {
        const { onTimeout } = this.props;
        this.triggered = true;
        if (onTimeout != null) onTimeout();
      }
      this.forceUpdate();
    }, 1000);
    ['click', 'keydown', 'touchstart'].forEach(type => {
      window.addEventListener(type, this.handleEvent);
    });
  }
  componentWillUnmount() {
    clearInterval(this.interval);
    ['click', 'keydown', 'touchstart'].forEach(type => {
      window.removeEventListener(type, this.handleEvent);
    });
  }
  render() {
    const timeRemaining = this.state.expireAt - Date.now();
    return (
      <div className={classNames('timeout-overlay-component', {
        hidden: timeRemaining > 5000,
      })}>
        <h1 className='title'>
          { __('timeoutTitle', [
            <span className='bold' key='remaining'>
              { __('timeoutRemaining', [
                <span className='seconds' key='seconds'>
                  { Math.max(0, Math.round(timeRemaining / 1000)) }
                </span>,
              ], false) }
            </span>,
          ], false) }
        </h1>
        <p className='text'>
          { __('timeoutText') }
        </p>
      </div>
    );
  }
}

TimeoutOverlay.propTypes = {
  timeout: PropTypes.number.isRequired,
  onTimeout: PropTypes.func,
};
