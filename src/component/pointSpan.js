import React from 'react';
import PropTypes from 'prop-types';

import __ from '../lang';

import '../style/component/pointSpan.css';

export default function PointSpan({ points }) {
  return (
    <span className='point-span-component'>
      { __('pointNum', [points]) }
    </span>
  );
}

PointSpan.propTypes = {
  points: PropTypes.number.isRequired,
};

