import React from 'react';
import PropTypes from 'prop-types';

import Slider from 'react-slick';

import '../style/component/couponList.css';

import CouponEntry from './couponEntry';

export default function CouponList({ currentPoints, coupons, onUse }) {
  if (coupons.length === 0) return false;
  return (
    <Slider autoplay autoplaySpeed={3000} arrows={false}
      className='coupon-list'
    >
      { coupons.map((coupon, i) => (
        <div key={i} className='coupon-list-entry'>
          <CouponEntry currentPoints={currentPoints} coupon={coupon}
            onUse={onUse.bind(null, coupon)} />
        </div>
      )) }
    </Slider>
  );
}

CouponList.propTypes = {
  currentPoints: PropTypes.number,
  coupons: PropTypes.array.isRequired,
  onUse: PropTypes.func,
};
