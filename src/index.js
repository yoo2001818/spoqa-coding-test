import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';

import MockConnector from './connector/mock';

import './style/index.css';

import Provider from './container/provider';

// Since react-hot-loader should be able to replace this, 'require' is used
// instead.
let App = require('./container/app').default;

// Create a new mock connector. If an actual connector is used, it should be
// created at here.
let connector = new MockConnector();

// Create a container DOM node and render the app inside.
let container = document.createElement('div');
container.id = 'root';
document.body.appendChild(container);

function renderApp() {
  render(
    <AppContainer>
      <Provider connector={connector}>
        <App />
      </Provider>
    </AppContainer>,
    container,
  );
}

renderApp();

// Needed for hot module replacement
if (module.hot) {
  module.hot.accept('./container/app', () => {
    App = require('./container/app').default;
    renderApp();
  });
}
