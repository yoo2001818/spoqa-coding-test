import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import '../style/container/app.css';

import SplashPage from './page/splash';
import FormPage from './page/form';
import ResultPage from './page/result';
import PageRouter from './pageRouter';
import ErrorDisplay from './errorDisplay';
import LanguageSet from './languageSet';
import LoadingOverlay from '../component/loadingOverlay';

import { fetchStoreInfo } from '../action';

const PAGES = {
  splash: SplashPage,
  form: FormPage,
  result: ResultPage,
};

export default class App extends PureComponent {
  componentDidMount() {
    // Fetch store information from the connector
    this.props.dispatch(fetchStoreInfo());
  }
  render() {
    const { dispatch, state } = this.props;
    const pageProps = { dispatch, state };
    const { page, store, loading } = state;
    // TODO Handle error - show snackbar, or any equivalent
    return (
      <LanguageSet language={store && store.language}>
        <div className='app-component'>
          <LoadingOverlay hidden={loading <= 0} />
          <ErrorDisplay {...pageProps} />
          <PageRouter value={page} pages={PAGES} pageProps={pageProps} />
        </div>
      </LanguageSet>
    );
  }
}

App.propTypes = {
  dispatch: PropTypes.func,
  state: PropTypes.object,
};
