import { PureComponent, cloneElement } from 'react';
import PropTypes from 'prop-types';

import { init, showError, startLoad, stopLoad } from '../action';

export default class Provider extends PureComponent {
  constructor(props) {
    super(props);
    this.mounted = false;
    this.state = {};
    // Dispatch function is extremely widely used throughout the application -
    // thus it'd be better to bind this function to this object.
    this.dispatch = this.dispatch.bind(this);
    // Initialize the state.
    this.dispatch(init());
  }
  dispatch(mutator) {
    // We're using simple knock-off version of redux.
    // The dispatch function should be distributed to other parts of the
    // application - thus it is applicable to use context.
    // However, the app itself it so simple that it doesn't require context
    // - it only adds more complexity.

    // Mutator can be a function, or an object.
    let result;
    if (typeof mutator === 'function') {
      // A getState function is not required for now - it just sends current
      // state of the application.
      result = mutator(this.dispatch, this.state, this.props);
    } else {
      result = mutator;
    }

    if (typeof result !== 'object') {
      throw new Error('mutator must return an object');
    }

    // Handle Promises. Otherwise, just set the result.
    if (typeof result.then === 'function') {
      this.dispatch(startLoad());
      result.then(this.setDispatchResult.bind(this), err => {
        console.error(err);
        this.dispatch(showError({ translate: 'genericError' }));
      }).then(() => this.dispatch(stopLoad()));
    } else {
      this.setDispatchResult(result);
    }
  }
  setDispatchResult(result) {
    console.log(result);
    if (this.mounted) this.setState(result);
    else this.state = Object.assign({}, this.state, result);
  }
  componentWillMount() {
    this.mounted = true;
  }
  componentWillUnmount() {
    this.mounted = false;
  }
  render() {
    const { children } = this.props;
    const { dispatch, state } = this;
    return cloneElement(children, { dispatch, state });
  }
}

Provider.propTypes = {
  connector: PropTypes.object,
  children: PropTypes.node.isRequired,
};
