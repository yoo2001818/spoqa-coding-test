import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import __ from '../lang';

import { closeError } from '../action';

import SnackBar from '../component/ui/snackBar';

export default class ErrorDisplay extends PureComponent {
  constructor(props) {
    super(props);
    this.registeredListeners = false;
    this.timer = null;
    this.handleClose = this.handleClose.bind(this);
  }
  handleClose() {
    this.props.dispatch(closeError());
  }
  toggleListeners(enabled) {
    if (enabled === this.registeredListeners) return;
    this.registeredListeners = enabled;
    ['keydown', 'mousedown', 'touchstart'].forEach(type => {
      if (enabled) window.addEventListener(type, this.handleClose);
      else window.removeEventListener(type, this.handleClose);
    });
  }
  updateTimer() {
    const { state: { error } } = this.props;
    const isOpen = error != null && error.open;
    // Install timeout handlers.
    clearTimeout(this.timer);
    if (isOpen && error.closeAt != null) {
      this.timer = setTimeout(this.handleClose, error.closeAt - Date.now());
    }
    // Toggle other handlers.
    this.toggleListeners(isOpen);
  }
  componentDidMount() {
    this.updateTimer();
  }
  componentDidUpdate() {
    this.updateTimer();
  }
  componentWillUnmount() {
    if (this.timer != null) clearTimeout(this.timer);
    this.toggleListeners(false);
  }
  render() {
    const { state: { error } } = this.props;
    const isOpen = error != null && error.open;
    const content = error != null && (error.message.translate != null
      ? __(error.message.translate, error.message.params)
      : error.message);
    return (
      <SnackBar hidden={!isOpen}>
        { content }
      </SnackBar>
    );
  }
}

ErrorDisplay.propTypes = {
  dispatch: PropTypes.func,
  state: PropTypes.object,
};
