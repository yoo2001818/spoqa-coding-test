import React from 'react';
import PropTypes from 'prop-types';

import __ from '../../lang';

import '../../style/container/page/result.css';

import Pane from '../../component/ui/pane';
import PointSpan from '../../component/pointSpan';
import TimeoutOverlay from '../../component/timeoutOverlay';
import CouponList from '../../component/couponList';

import { reset } from '../../action';

export default function ResultPage({ state: { result, store }, dispatch }) {
  return (
    <div className='result-page-component page-component'>
      <TimeoutOverlay timeout={10000} onTimeout={() => dispatch(reset())} />
      <Pane className='summary'>
        <div className='header'>
          <div className='points'>
            <PointSpan points={result.givenPoints} />
          </div>
          <div className='caption'>
            { __('resultDone') }
          </div>
        </div>
        <hr className='divider' />
        <div className='current'>
          { __('resultCurrent', [
            <PointSpan points={result.points} key='points' />,
          ], false) }
        </div>
      </Pane>
      <CouponList currentPoints={result.points} coupons={store.coupons}
        onUse={coupon => {
          // TODO Perform actual logic
          window.alert(coupon.name + '을(를) 교환했습니다!');
        }} />
    </div>
  );
}

ResultPage.propTypes = {
  dispatch: PropTypes.func,
  state: PropTypes.object,
};
