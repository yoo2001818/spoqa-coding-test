import React from 'react';
import PropTypes from 'prop-types';

import __ from '../../lang';

import '../../style/container/page/splash.css';

import ImageCarousel from '../../component/ui/imageCarousel';
import Logo from '../../component/ui/logo';
import Button from '../../component/ui/button';
import Marquee from '../../component/ui/marquee';

import { start } from '../../action';

export default function SplashPage({ state: { store }, dispatch }) {
  // TODO If store is null, show a loading screen
  if (store == null) return false;
  return (
    <div className='splash-page-component page-component'>
      <ImageCarousel images={store.images} className='content'
        autoplay autoplaySpeed={3000} />
      <div className='footer'>
        <div className='content'>
          <Logo className='logo' />
          <div className='title'>
            <Marquee speed={100}>{ store.name }</Marquee>
          </div>
        </div>
        <Button className='continue' onClick={() => dispatch(start())}>
          { __('collectPoint') }
        </Button>
      </div>
    </div>
  );
}

SplashPage.propTypes = {
  dispatch: PropTypes.func,
  state: PropTypes.object,
};
