import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import __ from '../../lang';

import '../../style/container/page/form.css';

import Header from '../../component/ui/header';
import CancelButton from '../../component/ui/cancelButton';
import Pane from '../../component/ui/pane';
import PointSpan from '../../component/pointSpan';
import PhoneInput from '../../component/phoneInput';
import TimeoutOverlay from '../../component/timeoutOverlay';

import kakaoImage from '../../image/kakao.png';

import { submit, reset } from '../../action';

export default class FormPage extends PureComponent {
  handleSubmit(phone) {
    if (this.props.state.loading > 0) return;
    this.props.dispatch(submit(phone));
  }
  render() {
    const { state: { form }, dispatch } = this.props;
    return (
      <div className='form-page-component page-component'>
        <TimeoutOverlay timeout={10000} onTimeout={() => dispatch(reset())} />
        <Header className='header' right={
          <CancelButton onClick={() => dispatch(reset())} />
        }>
          { __('phoneFormTitle') }
        </Header>
        <div className='content'>
          <div className='sidebar'>
            <Pane>
              <div className='overview-content'>
                { __('phoneFormOverview', [
                  <PointSpan points={form.points} key='points' />,
                ], false) }
              </div>
            </Pane>
            <Pane>
              <img src={kakaoImage} className='kakao-logo' alt='카카오톡 로고' />
              <div className='kakao-content'>
                { __('phoneFormKakao') }
              </div>
            </Pane>
          </div>
          <div className='content'>
            <PhoneInput className='phone-input'
              caption={__('phoneFormAgreement', [
                (
                  <a href='http://google.com' target='_blank' key='tos'>
                    { __('tos') }
                  </a>
                ),
                (
                  <a href='http://google.com' target='_blank' key='privacy'>
                    { __('privacyPolicy') }
                  </a>
                ),
              ], false)}
              doneLabel={__('phoneFormDone')}
              onSubmit={this.handleSubmit.bind(this)}
              registerWindowKeyEvent />
          </div>
        </div>
      </div>
    );
  }
}

FormPage.propTypes = {
  dispatch: PropTypes.func,
  state: PropTypes.object,
};
