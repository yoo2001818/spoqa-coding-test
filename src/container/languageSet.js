import { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { hasLanguage, setLanguage } from '../lang';

export default class LanguageSet extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { lang: null, reset: false };
    this.setLanguage(props.language, true);
    this.handleHashUpdate = this.setLanguage.bind(this, null, false);
  }
  componentDidMount() {
    window.addEventListener('hashchange', this.handleHashUpdate);
  }
  componentWillUnmount() {
    window.removeEventListener('hashchange', this.handleHashUpdate);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.language !== this.props.language) {
      this.setLanguage(nextProps.language);
    }
  }
  componentDidUpdate() {
    if (this.state.reset) {
      this.setState({ reset: false });
    }
  }
  setLanguage(lang, initial = false) {
    let setLang = lang;
    if (location.hash !== '') {
      let hash = location.hash.slice(1);
      if (hasLanguage(hash)) setLang = hash;
    }
    if (setLang != null) {
      setLanguage(setLang);
      if (initial) {
        this.state = { lang: setLang, reset: false };
      } else {
        this.setState({ lang: setLang, reset: true });
      }
    }
  }
  render() {
    if (this.state.reset) return false;
    return this.props.children;
  }
}

LanguageSet.propTypes = {
  language: PropTypes.string,
  children: PropTypes.node,
};
