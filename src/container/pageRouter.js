import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { CSSTransition } from 'react-transition-group';

import '../style/container/pageRouter.css';

// Duration is fixed to 500ms (in CSS)
const DURATION = 500;

export default class PageRouter extends PureComponent {
  render() {
    const { value, pages, pageProps } = this.props;
    // Show pages. Since the pages are required to have an animation,
    // we can't simply extract the page to use - rendering all pages is
    // required.
    // TODO Integrate animation
    return (
      <div className='page-router-component'>
        { Object.keys(pages).map(key => {
          const Page = pages[key];
          return (
            <CSSTransition classNames='fade' key={key}
              in={key === value} timeout={DURATION}
            >
              { state => (
                <div className={classNames('page', key === value && 'active')}>
                  { state !== 'exited' && (
                    <Page {...pageProps} />
                  ) }
                </div>
              ) }
            </CSSTransition>
          );
        }) }
      </div>
    );
  }
}

PageRouter.propTypes = {
  value: PropTypes.string,
  pages: PropTypes.object,
  pageProps: PropTypes.object,
};
