import * as config from '../../config';
// import delay from '../util/delay';

export default class MockConnector {
  async fetchStoreInfo() {
    // await delay(Math.random() * 4000);
    return config.store;
  }
  async collectPoint(phone, givenPoints) {
    // await delay(Math.random() * 4000);
    // It uses LocalStorage to store the current point information.
    // 1. Unpack localStorage information.
    let pointsData = {};
    try {
      pointsData = JSON.parse(window.localStorage.pointsData);
    } catch (e) {
      // Load failed - just continue with an empty object.
    }
    // 2. Query the current phone number and increment points.
    pointsData[phone] = (pointsData[phone] || 0) + givenPoints;
    // 3. Store info back to localStorage.
    window.localStorage.pointsData = JSON.stringify(pointsData);
    return { phone, points: pointsData[phone] };
  }
}
