import validatePhone from './validatePhone';

describe('validatePhone', () => {
  it('should accept mobile phone number', () => {
    expect(validatePhone('010-1234-5678')).toBe(true);
    expect(validatePhone('011-1234-5678')).toBe(true);
    expect(validatePhone('012-1234-5678')).toBe(true);
    expect(validatePhone('019-1234-5678')).toBe(true);
  });
  it('should not accept any other number', () => {
    expect(validatePhone('02-123-5678')).toBe(false);
    expect(validatePhone('02-1234-5678')).toBe(false);
    expect(validatePhone('070-1234-5678')).toBe(false);
    expect(validatePhone('119')).toBe(false);
    expect(validatePhone('154-1')).toBe(false);
    expect(validatePhone('007-00')).toBe(false);
  });
});
