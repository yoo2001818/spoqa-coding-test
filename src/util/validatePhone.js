export default function validatePhone(phone) {
  return /^01\d-(\d{4})-(\d{4})$/.test(phone);
}
