import formatPhone from './formatPhone';

describe('formatPhone', () => {
  it('should format unformatted number', () => {
    // 3-(3|4)-4 format is assumed.
    expect(formatPhone('01012345678')).toEqual('010-1234-5678');
    expect(formatPhone('0101235678')).toEqual('010-123-5678');
    expect(formatPhone('010123456')).toEqual('010-123-456');
    expect(formatPhone('01012345')).toEqual('010-123-45');
    expect(formatPhone('0101234')).toEqual('010-123-4');
    expect(formatPhone('010123')).toEqual('010-123');
  });
  it('should ignore overflown number', () => {
    expect(formatPhone('01012345678912345')).toEqual('010-1234-5678912345');
  });
  it('should reformat formatted number', () => {
    expect(formatPhone('010/1234/5678')).toEqual('010-1234-5678');
    expect(formatPhone('010-1234-5678')).toEqual('010-1234-5678');
    expect(formatPhone('010-123-45678')).toEqual('010-1234-5678');
  });
  it('should remove non numeric characters ', () => {
    expect(formatPhone('안녕abcd1234_!-@?56789')).toBe('123-456-789');
  });
});
