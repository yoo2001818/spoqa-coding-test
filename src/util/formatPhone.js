export default function formatPhone(phone) {
  let input = String(phone).replace(/[^0-9]/g, '');
  if (input.length >= 11) {
    return input.replace(/^(\d{3})(\d{4})(\d+)$/, '$1-$2-$3');
  } else if (input.length >= 7) {
    return input.replace(/^(\d{3})(\d{3})(\d+)$/, '$1-$2-$3');
  } else {
    return input.replace(/^(\d{3})(\d+)$/, '$1-$2');
  }
}
