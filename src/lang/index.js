import * as langData from './data';
// For convinence, global settings are used to set the current language.
// This is usually no problem at all, since the language rarely changes and
// everything is completely synchronous - it's not a problem to perform SSR
// too.

// If language data is extracted to separate files, 'setLanguage' function
// should be an async function.

let CURRENT_LANGUAGE = 'en';
let CURRENT_DATA = langData[CURRENT_LANGUAGE];

export function hasLanguage(language) {
  return langData[language] != null;
}

export function setLanguage(language) {
  if (language === CURRENT_LANGUAGE) return;
  if (langData[language] == null) {
    throw new Error('Language ' + language + ' is missing');
  }
  CURRENT_LANGUAGE = language;
  CURRENT_DATA = langData[language];
}

export default function translate(key, params = [], join = true,
  renderText = v => v,
) {
  // Split includes captured group if capture group is specified -
  // MDN specifies that this is not implemented in all browsers, however,
  // I couldn't find any browser that doesn't implement this.
  let input = (CURRENT_DATA[key] || `{${key}}`).split(/\$(\d+)/g);
  let result = input.map((v, i) => {
    if (i % 2 === 0) return renderText(v, i);
    let index = parseInt(v) - 1;
    return params[index];
  });
  if (join) return result.join('');
  return result;
}
