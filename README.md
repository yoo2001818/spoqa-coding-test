# spoqa-coding-test
A simple Dodo Point clone for Spoqa coding test

[Prebuilt website](https://kkiro.kr/spoqa-coding-test/)

## Setup
```
npm install
```

## Commands
- **test**: `npm test`
- **build**: `npm run build`
- **devServer**: `npm start`

## Configuration
You can change `config/index.js` to set the details of the website.

## Supported locales
- `en` - English
- `ko` - Korean

## Tested browsers
- Firefox 57 on Linux
- Chrome 63 on Linux
- Samsung Browser on Galaxy S5
- Mobile Chrome on Galaxy S5

## Features
- **Everything** specified on the requirements document
- Async error handling / loading overlay  
  Enable fake delay, or throw an error in `src/connector/mock.js` to see it
  in action.
- Multiple coupon entries  
  Add more coupon data in `config/index.js`.
- Setting locale by document hash  
  Append local code like this: `http://localhost:8080/#en`
- Landscape smartphone support  
  The whole website shrinks in small screen. (However, portrait mode doesn't
  work.)

## Note

### About Redux
I tried to avoid using Redux because the app is relatively simple, however,
I ended up making a simple knock-off version of redux.
Since there is no reducer, action handles everything - action acts as
a mutator.

Unlike (vanilla) Redux, action can be asynchronous too, and the provider will
automatically add loading / error state for it.

Actions can be found in `src/action/index.js`.

### SVG Icons
Since I had to create icons quickly, I used SVG icons, embedded to React
itself.

Refer `src/image/*.svg` and `src/action/component/ui/icon.js`.

### Components and Containers
Since there is no Redux, it is pretty meaningless to distinguish components and
containers. Still, I tried to separate them by whether if they actually are
tied to app state.

### Internationalization Support
i18n support is done by simple language switcher located in
`src/lang/index.js`.

It's usually called like: `__('test', ['Hello', 'World'])`.

However, I needed a way to embed hyperlinks and other texts into the i18n text.
Instead of using raw HTML, I made translation encoder to accept any objects
and return an array containing them. This way, React components could be
embedded to the i18n script.

See this code snippet:

```js
<div>
  { __('phoneFormAgreement', [
      <a href='http://google.com' target='_blank' key='tos'>
        { __('tos') }
      </a>,
      <a href='http://google.com' target='_blank' key='privacy'>
        { __('privacyPolicy') }
      </a>,
    ], false) }
</div>
```

### Text overflow

![Text overflow](./doc/textoverflow.png)

As you can see, the result screen's coupon entry display properly handles
text overflow like this.

This was pretty hard to implement since i18n code should be able to change
the order of them.

Continued from above, translation encoder can also wrap raw text with span -
thus it can use flexbox to layout the spans.

For example, `Hello, $1!` can be converted to:

```js
[
  <span className='plain-text' key={0}>Hello, </span>,
  /* anything provided by the user */,
  <span className='plain-text' key={1}>!</span>,
]
```
